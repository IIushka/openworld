#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "OpenWorld/AnimInstance/HumanAnimInstance.h"
#include "OpenWorld/Interfaces/HumanInterface.h"
#include "CH_Human.generated.h"

UCLASS()
class OPENWORLD_API ACH_Human : public ACharacter, public IHumanInterface
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Component, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* swordStrap;	// ��������� �� �����
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Component, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* handSocket;	// ��������� � ����

public:
	ACH_Human();

public:	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override; 

protected:
	virtual void Landed(const FHitResult& Hit) override;
	

// �������
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	void MoveForward(float Value);
	void MoveRight(float Value);	
	UHumanAnimInstance* HumanAnimInstance = nullptr;

public:
	void ToggleRun();
	void EnableSprint();
	void DisableSprint();
	void ToggleCrouch();
	void Jump();

	void SendCrouchStatus(bool status);
	void SendEquippedStatus(bool status);

	void ToggleSword();
	void AttachSwordToHand();
	void AttachSwordToStrap();
	void EndEquippedTimer();
	void EndDeEquippedTimer();
	void Attack();
	bool CheckNotPlayingMontages(); 
	void EndAttackedTime();
	void EndComboAttackTime();
	void StartBlock();
	void EndBlock();
	void LockOn();	
	void LockOnEnemy(bool lock);
	void FindEnemyes();
	void TargetIsEnemy(ACH_Human* targetEnemy);
	void WeaponHit(ACH_Human* targetEnemy, float dmamge);
	void GetHit(float dmamge);
	void Death();

	UFUNCTION()
		void BeginOverlapBody(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

// ����������
public:
	float runningSpeed = 600.f;
	float walkingSpeed = 300.f;
	float sprintingSpeed = 800.f;
	bool running = true;
	bool sprinting = false;
	bool crouching = false;
	bool equipped = false;
	int8 comboCounter = 0;
	bool canMove = true;	
	bool prepareToBlock = false;
	bool blocking = false;
	UPROPERTY(EditAnywhere, Category = "Commands")
		int8 command = 1;
	bool lockOnEnemy = false;
	ACH_Human* lockedEnemy = nullptr;
	bool canHit = true;
	float health = 100.f;


// �������
public:
	FTimerHandle AttachTimer;	// ������ �������� ������
	FTimerHandle EquippedTimer;	// ������ ����������
	FTimerHandle AttackedTime;	// ������ �����
	FTimerHandle ComboAttackTimer;	// ������ �����-����
	FTimerHandle AtackNonTarget;	// 
	FTimerHandle BlockReactTimer;	// ������ �������� ������� �� ���� ��� �����


// ����������
	virtual void ToggleMovement_Implementation(bool status) override;
	virtual void ToggleWeaponCollision_Implementation(bool status) override;


//��������� ���������� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		UStaticMeshComponent* Weapon = nullptr;

// ��������� ���������� ������������ ��������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* JumpMontage = nullptr;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* EquipSword = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* SwordAttack = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* PowerSwordAttack = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* ComboSwordAttack = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* BlockMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* HitMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* BlockReactMontage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Montages")
		UAnimMontage* DeathMontage = nullptr;
};
