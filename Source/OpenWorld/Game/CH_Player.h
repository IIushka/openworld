#pragma once

#include "CoreMinimal.h"
#include "OpenWorld/Game/CH_Human.h"
#include "CH_Player.generated.h"


UCLASS()
class OPENWORLD_API ACH_Player : public ACH_Human
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;	// ������ ������
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;	// ������	

public:
	ACH_Player();
};
