#include "OpenWorld/Game/CH_Human.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"


ACH_Human::ACH_Human()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);	// ������ ������� ���������

	//	���������� �������� ��������� �� ������������
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// ��������� ������������ ���������
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxAcceleration = 500.f;
	GetCharacterMovement()->GroundFriction = 1.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 100.f;
	GetCharacterMovement()->JumpZVelocity = 350.f;

	// ������������� ��������� ���� ���������
	GetMesh()->SetWorldLocation(FVector(0.f, 0.f, -95.f));
	GetMesh()->SetWorldRotation(FRotator(0.f, -90.f, 0.f));

	// ��������� �������
	swordStrap = CreateDefaultSubobject<USceneComponent>(TEXT("SwordStrap"));
	swordStrap->SetupAttachment(GetMesh(), FName("SwordSpineSocket"));
	handSocket = CreateDefaultSubobject<USceneComponent>(TEXT("HandSocket"));
	handSocket->SetupAttachment(GetMesh(), FName("SwordInHandSocket"));

	// ��������� ��������� ������
	Weapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	Weapon->SetupAttachment(swordStrap);
	Weapon->SetCollisionProfileName(TEXT("OverlapAll"));
	Weapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}


void ACH_Human::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// ��������� �������� ������ ��������
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACH_Human::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACH_Human::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction("ToggleRun", IE_Pressed, this, &ACH_Human::ToggleRun);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ACH_Human::EnableSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ACH_Human::DisableSprint);
	PlayerInputComponent->BindAction("ToggleCrouch", IE_Pressed, this, &ACH_Human::ToggleCrouch);
	
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACH_Human::Jump);

	PlayerInputComponent->BindAction("ToggleSword", IE_Pressed, this, &ACH_Human::ToggleSword);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ACH_Human::Attack);
	PlayerInputComponent->BindAction("Block", IE_Pressed, this, &ACH_Human::StartBlock);
	PlayerInputComponent->BindAction("Block", IE_Released, this, &ACH_Human::EndBlock);
	PlayerInputComponent->BindAction("LockOn", IE_Pressed, this, &ACH_Human::LockOn);
}

void ACH_Human::Landed(const FHitResult& Hit)
{
	if (JumpMontage)
		PlayAnimMontage(JumpMontage, 1.f, "Land");
}

void ACH_Human::BeginPlay()
{
	Super::BeginPlay();

	HumanAnimInstance = Cast<UHumanAnimInstance>(GetMesh()->GetAnimInstance());
	if (!HumanAnimInstance)
		UE_LOG(LogTemp, Warning, TEXT("Don't find HumanAnimInstance in BeginPlay!"));

	Weapon->OnComponentBeginOverlap.AddDynamic(this, &ACH_Human::BeginOverlapBody);
}

void ACH_Human::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (lockOnEnemy)	// ���� ����������� �� ����
	{
		if (lockedEnemy)	// ���� ���� ����������
		{	
			// ������� ���� �������� � ���� � ������ ��������� ���� ����
			FRotator look = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), lockedEnemy->GetActorLocation());
			SetActorRotation(FRotator(GetActorRotation().Pitch, look.Yaw, GetActorRotation().Roll));
		}
	}
}

void ACH_Human::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && canMove)
	{
		// ����� ���� �����
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// ��������� ������� �������
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);	// ���������� �������� � ���� �����������
	}
}

void ACH_Human::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && canMove)
	{
		// ���������� ���� �������
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// ��������� ������� �������
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);	// ���������� �������� � ���� �����������
	}
}

void ACH_Human::ToggleRun()
{
	running = !running;

	if (running)
	{
		GetCharacterMovement()->MaxWalkSpeed = runningSpeed;
		crouching = false;	// ���������� �������
		SendCrouchStatus(crouching);	// ����������� ������� � �������
	}
	else
		GetCharacterMovement()->MaxWalkSpeed = walkingSpeed;
}

void ACH_Human::EnableSprint()
{
	if (CheckNotPlayingMontages())	// �������� �� �������� �������
	{
		sprinting = true;
		running = true;
		crouching = false;	// ���������� �������
		if(equipped)
			ToggleSword();	// ������ ���
		SendCrouchStatus(crouching);	// ����������� ������� � �������
		GetCharacterMovement()->MaxWalkSpeed = sprintingSpeed;
	}
}

void ACH_Human::DisableSprint()
{
	if (CheckNotPlayingMontages() && !equipped)	// �������� �� �������� �������
	{
		sprinting = false;
		if (!crouching)	// ���� �� ����� ������, ���� ���������� �������� � �������
			GetCharacterMovement()->MaxWalkSpeed = runningSpeed;
	}
}

void ACH_Human::ToggleCrouch()
{
	crouching = !crouching;
	SendCrouchStatus(crouching);	// ����������� ������� � �������
	if (crouching)	// ���� � �������
	{
		GetCharacterMovement()->MaxWalkSpeed = walkingSpeed; 
		running = false;	// ���������� ����
		sprinting = false;	// ���������� �������
	}
	else	// ���� ������
	{
		GetCharacterMovement()->MaxWalkSpeed = runningSpeed;
		running = true;
	}
}

void ACH_Human::Jump()
{
	ACharacter::Jump();
	if (JumpMontage)
		PlayAnimMontage(JumpMontage);
}

void ACH_Human::SendCrouchStatus(bool status)	// �������� ���� �������� ��������� �������
{
	HumanAnimInstance->crouching = status;	
}

void ACH_Human::SendEquippedStatus(bool status)
{
	HumanAnimInstance->equipped = status;
}

void ACH_Human::ToggleSword()
{
	if (CheckNotPlayingMontages())	// �������� �� �������� �������
	{
		if (!equipped)	// ���� �������� �� ����������
		{
			float timeMontage = (PlayAnimMontage(EquipSword));
			GetWorld()->GetTimerManager().SetTimer(AttachTimer, this, &ACH_Human::AttachSwordToHand, 0.6f, false);	// ����� ����������� � ����
			GetWorld()->GetTimerManager().SetTimer(EquippedTimer, this, &ACH_Human::EndEquippedTimer, timeMontage, false);	// ������ ��������� ����������
			GetCharacterMovement()->MaxWalkSpeed = walkingSpeed;
		}
		else	// ���� �������� ����������
		{
			float timeMontage = (PlayAnimMontage(EquipSword));
			GetWorld()->GetTimerManager().SetTimer(AttachTimer, this, &ACH_Human::AttachSwordToStrap, 0.6f, false);	// ����� ����������� � �����
			GetWorld()->GetTimerManager().SetTimer(EquippedTimer, this, &ACH_Human::EndDeEquippedTimer, timeMontage, false);	// ������ ��������� ������������
			GetCharacterMovement()->MaxWalkSpeed = runningSpeed;
		}
	}
}

void ACH_Human::AttachSwordToHand()
{
	GetWorld()->GetTimerManager().ClearTimer(AttachTimer);	// ������ ������
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, handSocket->GetAttachSocketName());	// ������� ������ � ����
	equipped = true;	// �������� ����� � ��� (��������� ������ ��������)
	SendEquippedStatus(true);
}

void ACH_Human::AttachSwordToStrap()
{
	GetWorld()->GetTimerManager().ClearTimer(AttachTimer);	// ������ ������
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, swordStrap->GetAttachSocketName());	// ������� ������ � �����
	equipped = false;	// �������� �� ����� � ���
	SendEquippedStatus(false);
}

void ACH_Human::EndEquippedTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(EquippedTimer);	// �������� �������� ����������	
	if (prepareToBlock)	// ���� ���� �� ��� �����
		StartBlock();	// �������� ����
}

void ACH_Human::EndDeEquippedTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(EquippedTimer);	// �������� �������� ������������
}

void ACH_Human::Attack()
{
	if (CheckNotPlayingMontages())	// �������� �� �������� �������
	{
		if (equipped)	// ���� �������� ���������� �������
		{
			comboCounter++;	// ������� �����
			float timeMontage = 0.f;
			switch (comboCounter)
			{
			case 1:
				timeMontage = (PlayAnimMontage(SwordAttack, 1.6f, "None"));	// ������������ �������� �����
				GetWorld()->GetTimerManager().ClearTimer(ComboAttackTimer);	// ������ ������ �������� �����
				GetWorld()->GetTimerManager().SetTimer(ComboAttackTimer, this, &ACH_Human::EndComboAttackTime, timeMontage / 1.6f + 0.6f, false);	// ����� �������� + ����� ������������
				break;
			case 2:
				timeMontage = (PlayAnimMontage(PowerSwordAttack, 1.6f, "None"));	// ������������ �������� ��������� �����
				GetWorld()->GetTimerManager().ClearTimer(ComboAttackTimer);	// ������ ������ �������� �����
				GetWorld()->GetTimerManager().SetTimer(ComboAttackTimer, this, &ACH_Human::EndComboAttackTime, timeMontage / 1.6f + 0.3f, false);	// ����� �������� + ����� ������������
				break;
			case 3:
				timeMontage = (PlayAnimMontage(ComboSwordAttack, 1.6f, "None"));	// ������������ �������� �����-�����
				GetWorld()->GetTimerManager().ClearTimer(ComboAttackTimer);	// ������� �������
				comboCounter = 0;	// ����� �����-�����
				break;
			}
			GetWorld()->GetTimerManager().SetTimer(AttackedTime, this, &ACH_Human::EndAttackedTime, timeMontage / 1.6f, false);	// ������� ��������� �������� �����
			Weapon->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

			if (!lockOnEnemy)	// ���� ��������� �� ��������
			{
				LockOn();	// ������� � ���������� ����������
				GetWorld()->GetTimerManager().SetTimer(AtackNonTarget, this, &ACH_Human::LockOn, timeMontage / 1.6f, false);
			}
		}
		else	// ���� �������� �� ���������� �������
		{
			ToggleSword();	// ����������
		}
	}
}

void ACH_Human::EndAttackedTime()
{
	GetWorld()->GetTimerManager().ClearTimer(AttackedTime);	// �������� �������� �����
	Weapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (prepareToBlock)	// ���� ���� �� ��� �����
		StartBlock();	// �������� ����
}

void ACH_Human::EndComboAttackTime()
{
	GetWorld()->GetTimerManager().ClearTimer(ComboAttackTimer);	// ������� �������
	comboCounter = 0;	// ����� �������� �����
}

void ACH_Human::StartBlock()
{
	if (CheckNotPlayingMontages())	// �������� �� �������� �������
	{
		prepareToBlock = true;	// ������ ������� �����
		if (!equipped)	// ���� �������� �� ���������� �������
			ToggleSword();	// ����������
		else
		{
			blocking = true;
			PlayAnimMontage(BlockMontage);
		}
	}
}

void ACH_Human::EndBlock()
{
	prepareToBlock = false;	// ������� ����� �� ������
	blocking = false;
	StopAnimMontage(BlockMontage);
}

void ACH_Human::LockOnEnemy(bool lock)
{
	if (lock)	// ���� �������� ��������
	{
		lockOnEnemy = true;	// ������ ���� ����������� � ���� ��������������� �� �����
		GetCharacterMovement()->bOrientRotationToMovement = false;
	}
	else	// ���� ���������
	{
		lockOnEnemy = false;	// ��������� ������ ��������
		GetCharacterMovement()->bOrientRotationToMovement = true;
	}
}


void ACH_Human::FindEnemyes()
{
	TArray<AActor*> ActorsToIgnore;	// ������������ ������
	ActorsToIgnore.Add(this);	// ������� ����
	TArray<FHitResult> hitResult;	// ��������� ���� ������

	/**	����� */
	bool status = UKismetSystemLibrary::SphereTraceMulti(GetWorld(), GetActorLocation(), GetActorLocation(), 1000.f,
		UEngineTypes::ConvertToTraceType(ECC_Pawn), false, ActorsToIgnore, EDrawDebugTrace::ForDuration,
		hitResult, false, FLinearColor::Red, FLinearColor::Green, 5.f);

	ACH_Human* targetEnemy = nullptr;	// ���������� ��� ������ �� �����
	if (status)	// ���� ��� �����
	{
		for (int i = 0; i < hitResult.Num() - 1; i++)
		{
			if (ACH_Human* HumanClass = Cast<ACH_Human>(hitResult[i].GetActor()))	// ���� ������ �������� ACH_Human
			{
				if (HumanClass->health > 0.f)
				{
					if (targetEnemy)	// ���� ��� �� ������ ������� �������
					{
						// ���� ��������� �� ����������� �������� ������ ��� �� ��������
						if (GetDistanceTo(targetEnemy) >= GetDistanceTo(HumanClass))
							targetEnemy = HumanClass;	//�� ���������� �������
					}
					else	// ���� ��� ������ ������� �������, �� ������� �����
						targetEnemy = HumanClass;	// ��������� ������� ��� �����
				}
			}
		}
		if (targetEnemy)	// ���� ������ ����� ������ ������ 
			TargetIsEnemy(targetEnemy);
	}
}

void ACH_Human::TargetIsEnemy(ACH_Human* targetEnemy)
{
	if (targetEnemy->command != command)	// �������� �� ������������ (���� ��� ����� ������)
	{
		lockedEnemy = targetEnemy;	// ���������� ������ ����������
		LockOnEnemy(true);	// �������� ����������
	}
}

void ACH_Human::WeaponHit(ACH_Human* targetEnemy, float damage)
{
	targetEnemy->GetHit(damage);	// ��������� �������� ����
}

void ACH_Human::GetHit(float damage)	// ���� ������� ����
{
	if(health > 0.f)
	{
		if (!blocking)
		{
			if (HitMontage)
				PlayAnimMontage(HitMontage);
			health -= damage;

			if (health <= 0.f)
				Death();
		}
		else if (BlockReactMontage)
		{
			float timeMontage = PlayAnimMontage(BlockReactMontage);
			GetWorld()->GetTimerManager().SetTimer(BlockReactTimer, this, &ACH_Human::StartBlock, timeMontage - 0.3f, false);	// ������� ��������� �������� ������� �� ����
		}
	}
}

void ACH_Human::Death()
{
	PlayAnimMontage(DeathMontage);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	canMove = false; 
}

void ACH_Human::BeginOverlapBody(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != this && canHit)	// ���� �� ��������� ���� � ���� �� ��� ������
		if (ACH_Human* HumanClass = Cast<ACH_Human>(OtherActor))	// � ���� ������ �������� ����������
		{
			canHit = false;	// ���������� ����������� ���������� ������������ ��������
			WeaponHit(HumanClass, 20.f);	// ��������� ����� �����
		}
}

void ACH_Human::LockOn()
{
	if (lockOnEnemy)	// ���� ��������� ��������
	{
		lockOnEnemy = false;	// ���������� �������
		LockOnEnemy(false);	// ��������� �������� �� �����
		lockedEnemy = nullptr;	// ������� ������� �������
	} 
	else	// ���� ��������� �� ��������
	{
		FindEnemyes();	// ����� �����������
	}
}

void ACH_Human::ToggleMovement_Implementation(bool status)
{
	canMove = status;
}

void ACH_Human::ToggleWeaponCollision_Implementation(bool status)
{
	canHit = status;
}

bool ACH_Human::CheckNotPlayingMontages()
{
	// ���� �� ��� ���������� ������� � �� ���������� �����
	if (!GetWorld()->GetTimerManager().IsTimerActive(EquippedTimer) && !GetWorld()->GetTimerManager().IsTimerActive(AttackedTime))
		return true;	// ������ �� ������
	else
		return false;	// ������ ������
}
