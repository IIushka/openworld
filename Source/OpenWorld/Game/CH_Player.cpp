#include "OpenWorld/Game/CH_Player.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


ACH_Player::ACH_Player()
{
	// �������� ������ ������
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // ��������� �� ������
	CameraBoom->bUsePawnControlRotation = true; // ��������� �������� ������ �� ������������

	// �������� ������
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false; // ���������� �������� ������ �� ������������
}
