#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "HumanInterface.generated.h"


UINTERFACE(MinimalAPI)
class UHumanInterface : public UInterface
{
	GENERATED_BODY()
};


class OPENWORLD_API IHumanInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Movement")
		void ToggleMovement(bool status);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Weapon")
		void ToggleWeaponCollision(bool status);
};
