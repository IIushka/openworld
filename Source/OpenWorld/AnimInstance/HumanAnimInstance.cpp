#include "OpenWorld/AnimInstance/HumanAnimInstance.h"


void UHumanAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	AActor* OwningActor = GetOwningActor();	// �������� ������ �� ������ ��������

	if (OwningActor)
	{
		// ��������� ������ � ����������� ��������
		speedForward = FVector::DotProduct(OwningActor->GetVelocity(), OwningActor->GetActorForwardVector());
		speedSide = FVector::DotProduct(OwningActor->GetVelocity(), OwningActor->GetActorRightVector());
	}
}
