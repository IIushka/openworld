#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "OpenWorld/Interfaces/HumanInterface.h"
#include "HumanAnimInstance.generated.h"


UCLASS()
class OPENWORLD_API UHumanAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		float speedForward = 0.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		float speedSide = 0.f;	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		bool crouching = false;	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		bool equipped = false;

private:
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
};
